{
  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        nativeBuildInputs = with pkgs;
          [ pkg-config ] ++ (with xorg; [ libX11 libXcursor libXi libXrandr ]);
      in {
        defaultPackage = naersk-lib.buildPackage {
          root = ./.;

          inherit nativeBuildInputs;
        };

        defaultApp = let
          drv = self.defaultPackage."${system}";
          name = pkgs.lib.strings.removeSuffix ("-" + drv.version) drv.name;
        in utils.lib.mkApp {
          inherit drv;
          # TODO: https://github.com/nix-community/naersk/issues/224
          exePath = "/bin/${name}";
        };

        devShell = with pkgs;
          mkShell {
            buildInputs =
              [ cargo rust-analyzer rustc rustfmt rustPackages.clippy ];

            RUST_SRC_PATH = rustPlatform.rustLibSrc;

            inherit nativeBuildInputs;
          };
      });
}
