use clap::{ArgEnum, Parser};
use color_eyre::eyre;
use i3_ipc::Connect;

/// Traverse the tree representing a workspace in i3/sway.
// TODO: Can I add a full-stop to the help and version messages?
#[derive(Debug, Parser)]
#[clap(author, version)]
struct Cli {
    /// Whether to move the container or focus a new one.
    #[clap(subcommand)]
    command: Command,
    /// Directory to place log files.
    #[clap(long, short = 'd')]
    log_directory: Option<std::path::PathBuf>,
    /// The minimum level of logs to print.
    #[clap(arg_enum, default_value_t = LogLevel::Info, long, short)]
    log_level: LogLevel,
}

/// Whether to move the container or focus a new one.
#[derive(Debug, clap::Subcommand)]
enum Command {
    /// Focus on a new container.
    Focus {
        /// The direction in which to focus.
        #[clap(arg_enum)]
        direction: Direction,
    },
    /// Move this container to a new position in the tree.
    Move {
        /// The direction in which to move.
        #[clap(arg_enum)]
        direction: Direction,
    },
}

/// The direction to focus or move the container.
#[derive(ArgEnum, Clone, Copy, Debug)]
enum Direction {
    /// To the level of the parent of this container.
    Parent,
    /// To the left of the current position.
    Left,
    /// To the right of the current position.
    Right,
    /// To the child of this container.
    ///
    /// `move child` operations will create a storage container between the
    /// current container and it's parent.
    Child,
    /// To the child of the left container to this one.
    ChildLeft,
    /// To the child of the right container to this one.
    ChildRight,
}

impl From<Direction> for &'static str {
    fn from(direction: Direction) -> Self {
        use Direction::*;

        match direction {
            Parent => "parent",
            Left => "left",
            Right => "right",
            Child => "child",
            ChildLeft => "child-left",
            ChildRight => "child-right",
        }
    }
}

/// The minimum level of logs to print.
#[derive(ArgEnum, Clone, Copy, Debug)]
enum LogLevel {
    Trace,
    Debug,
    Info,
    Warn,
    Error,
}

impl From<LogLevel> for &'static str {
    fn from(log_level: LogLevel) -> Self {
        use LogLevel::*;

        match log_level {
            Trace => "trace",
            Debug => "debug",
            Info => "info",
            Warn => "warn",
            Error => "error",
        }
    }
}

impl From<LogLevel> for flexi_logger::Duplicate {
    fn from(log_level: LogLevel) -> Self {
        use flexi_logger::Duplicate::*;

        match log_level {
            LogLevel::Trace => Trace,
            LogLevel::Debug => Debug,
            LogLevel::Info => Info,
            LogLevel::Warn => Warn,
            LogLevel::Error => Error,
        }
    }
}

fn coloured_log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    let level = record.level();
    // TODO: Make the level fixed width (`:5` doesn't work).
    write!(
        w,
        "[{}] {} [{}] {}",
        flexi_logger::style(level)
            .paint(now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK)),
        flexi_logger::style(level).paint(record.level().to_string()),
        record.module_path().unwrap_or("<unnamed>"),
        flexi_logger::style(level).paint(&record.args().to_string()),
    )
}

fn log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    write!(
        w,
        "[{}] {} [{}] {}",
        now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK),
        record.level(),
        record.module_path().unwrap_or("<unnamed>"),
        &record.args(),
    )
}

fn find_focused_container(i3: &mut i3_ipc::I3Stream) -> eyre::Result<i3_ipc::reply::Node> {
    let tree = i3.get_tree()?;

    let mut current = &tree;
    while !current.focused {
        // NOTE: These two lines are guaranteed not to panic, unless i3/sway fucks up.
        // We start at the root container, meaning that in the first iteration `.focus`
        // is guaranteed to be non-empty.
        let focus = current.focus[0];
        // i3/sway guarantees that `.focus` will only have IDs of child nodes, so `find`
        // is guaranteed to find the right child. i3/sway guarantees that by
        // following the left-most (index zero) node in `.focus` you will reach the
        // focused container, meaning that on subsequent iterations `.focus` is
        // guaranteed to be non-empty.
        current = current.nodes.iter().find(|node| node.id == focus).unwrap()
    }

    Ok(current.clone())
}

fn find_parent_of_focused_container(
    i3: &mut i3_ipc::I3Stream,
) -> eyre::Result<i3_ipc::reply::Node> {
    let tree = i3.get_tree()?;

    let mut parent = &tree;
    let focus = parent.focus[0];
    // NOTE: Root container should always have a child, I believe.
    let mut current = parent.nodes.iter().find(|node| node.id == focus).unwrap();
    while !current.focused {
        let focus = current.focus[0];
        parent = current;
        current = current.nodes.iter().find(|node| node.id == focus).unwrap();
    }

    Ok(parent.clone())
}

fn open_placeholder_window(i3: &mut i3_ipc::I3Stream) -> eyre::Result<()> {
    let event_loop: winit::event_loop::EventLoop<()> = winit::event_loop::EventLoop::new();
    let _ = winit::window::Window::new(&event_loop);

    log::debug!(
        "Placeholder window opened: {}",
        display_focused_container(i3)?,
    );

    Ok(())
}

fn display_container(node: &i3_ipc::reply::Node) -> String {
    let mut display = format!("{}", node.id);

    if let Some(id) = node.window {
        display.push_str(" [");
        display.push_str(&format!("{}", id));
        display.push(']');
    }

    if let Some(ref name) = node.name {
        display.push_str(" (");
        display.push_str(name);
        display.push(')');
    }

    display
}

fn display_focused_container(i3: &mut i3_ipc::I3Stream) -> eyre::Result<String> {
    Ok(display_container(&find_focused_container(i3)?))
}

fn focus_relative(i3: &mut i3_ipc::I3Stream, direction: Direction) -> eyre::Result<()> {
    use Direction::*;

    log::debug!("Focusing relative container: {}", Into::<&str>::into(direction));

    match direction {
        ChildLeft => {
            i3.run_command("focus prev sibling")?;
            i3.run_command("focus child")?;
        }
        ChildRight => {
            i3.run_command("focus next sibling")?;
            i3.run_command("focus child")?;
        }
        Left => {
            i3.run_command("focus prev sibling")?;
        }
        Right => {
            i3.run_command("focus next sibling")?;
        }
        _ => {
            i3.run_command(&format!("focus {}", Into::<&str>::into(direction)))?;
        }
    }

    log::trace!(
        "Newly focused container: {}",
        display_focused_container(i3)?,
    );

    Ok(())
}

fn focus_id_of(i3: &mut i3_ipc::I3Stream, container: &i3_ipc::reply::Node) -> eyre::Result<()> {
    log::debug!(
        "Focusing container by id: {}",
        display_container(container)
    );
    i3.run_command(&format!("[con_id={}] focus", container.id))?;
    log::trace!(
        "Newly focused container: {}",
        display_focused_container(i3)?
    );

    Ok(())
}

fn swap_focused_with_id_of(
    i3: &mut i3_ipc::I3Stream,
    container: &i3_ipc::reply::Node,
) -> eyre::Result<()> {
    log::debug!(
        "Swapping containers: {} -> {}",
        display_focused_container(i3)?,
        display_container(container),
    );
    i3.run_command(&format!("swap container with con_id {}", container.id))?;
    log::trace!(
        "Newly focused container: {}",
        display_focused_container(i3)?,
    );

    Ok(())
}

fn kill_focused(i3: &mut i3_ipc::I3Stream) -> eyre::Result<()> {
    log::debug!("Killing container: {}", display_focused_container(i3)?);
    i3.run_command("kill")?;

    Ok(())
}

fn layout_focused(
    i3: &mut i3_ipc::I3Stream,
    layout: i3_ipc::reply::NodeLayout,
) -> eyre::Result<()> {
    use i3_ipc::reply::NodeLayout::*;

    let layout_string = match layout {
        SplitH => "splith",
        SplitV => "splitv",
        Stacked => "stacking",
        Tabbed => "tabbed",
        other => {
            eyre::bail!(
                "Layout `{:?}` can't be used with the `layout` command!",
                other,
            );
        }
    };

    log::debug!(
        "Setting layout of focused container: {} -> {}",
        display_focused_container(i3)?,
        layout_string,
    );

    i3.run_command(&format!("layout {}", layout_string))?;

    Ok(())
}

fn layout_default_focused(i3: &mut i3_ipc::I3Stream) -> eyre::Result<()> {
    log::debug!(
        "Setting layout of focused container to default: {}",
        display_focused_container(i3)?,
    );
    i3.run_command("layout default")?;

    Ok(())
}

fn create_storage_between_focused_and_parent(i3: &mut i3_ipc::I3Stream) -> eyre::Result<()> {
    let parent = find_parent_of_focused_container(i3)?;
    log::debug!(
        "Parent of focused container: {}",
        display_container(&parent),
    );

    let restore_parent_to = parent.layout;
    log::trace!("Parent layout: {:?}", restore_parent_to);

    layout_focused(i3, i3_ipc::reply::NodeLayout::Tabbed)?;

    i3.run_command("split toggle")?;
    log::trace!(
        "New storage container created: {}",
        display_container(&find_parent_of_focused_container(i3)?),
    );

    layout_default_focused(i3)?;
    focus_relative(i3, Direction::Parent)?;
    layout_focused(i3, restore_parent_to)?;
    focus_relative(i3, Direction::Child)?;

    Ok(())
}

const QUALIFIER: &str = "xyz";
const ORGANISATION: &str = "seamsay";
const APPLICATION: &str = clap::crate_name!();

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    let cli = Cli::parse();

    let log_directory = cli.log_directory.clone().unwrap_or_else(|| {
        directories::ProjectDirs::from(QUALIFIER, ORGANISATION, APPLICATION)
            .expect("A setup where this returns `None` would not be able to run i3.")
            .state_dir()
            .expect("This directory always exists on Linux.")
            .into()
    });

    let log_level: &str = cli.log_level.into();

    flexi_logger::Logger::try_with_env_or_str(log_level)?
        .adaptive_format_for_stderr(flexi_logger::AdaptiveFormat::Custom(
            log_format,
            coloured_log_format,
        ))
        .format_for_files(log_format)
        .log_to_file(flexi_logger::FileSpec::default().directory(&log_directory))
        .duplicate_to_stderr(cli.log_level.into())
        .start()?;

    log::trace!("Arguments parsed: {:?}", cli);

    let mut i3 = i3_ipc::I3::connect()?;

    const TESTED_I3_VERSION: (usize, usize, usize) = (4, 20, 1);
    let i3_version = i3.get_version()?;
    log::trace!("Connected to i3: {:?}", i3_version);
    if i3_version.major < TESTED_I3_VERSION.0
        || (i3_version.major == TESTED_I3_VERSION.0 && i3_version.minor < TESTED_I3_VERSION.1)
        || (i3_version.major == TESTED_I3_VERSION.0
            && i3_version.minor == TESTED_I3_VERSION.1
            && i3_version.patch < TESTED_I3_VERSION.2)
    {
        log::warn!(
            "This program has not been tested on sway or i3 versions older than '{}.{}.{}'!",
            TESTED_I3_VERSION.0,
            TESTED_I3_VERSION.1,
            TESTED_I3_VERSION.2
        );
    }

    let focused = find_focused_container(&mut i3)?;
    log::debug!(
        "Currently focused container: {}",
        display_container(&focused),
    );

    {
        use Command::*;
        use Direction::*;

        match cli.command {
            Move { direction } => {
                let container_to_move = focused;
                match direction {
                    direction @ (Parent | ChildLeft | ChildRight) => {
                        focus_relative(&mut i3, direction)?;

                        let sibling = find_focused_container(&mut i3)?;
                        if sibling.nodes.is_empty() {
                            create_storage_between_focused_and_parent(&mut i3)?;
                        }

                        open_placeholder_window(&mut i3)?;
                        swap_focused_with_id_of(&mut i3, &container_to_move)?;
                        kill_focused(&mut i3)?;
                        focus_id_of(&mut i3, &container_to_move)?;
                    }
                    Child => {
                        create_storage_between_focused_and_parent(&mut i3)?;
                    }
                    direction @ (Left | Right) => {
                        focus_relative(&mut i3, direction)?;
                        let sibling = find_focused_container(&mut i3)?;
                        if sibling.id == container_to_move.id {
                            log::info!(
                                "Not moving container because it has no sibling in that \
                                 direction: {}",
                                display_container(&sibling),
                            );
                        } else {
                            swap_focused_with_id_of(&mut i3, &container_to_move)?;
                            focus_relative(&mut i3, direction)?;
                        }
                    }
                }
            }
            Focus { direction } => {
                focus_relative(&mut i3, direction)?;
            }
        }
    }

    Ok(())
}
